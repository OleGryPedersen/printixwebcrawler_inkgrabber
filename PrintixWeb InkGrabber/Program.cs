﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.IO;

using HtmlAgilityPack;
using Newtonsoft.Json;

namespace PrintixWeb
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>

        private class toner 
        {
            public string descr;
            public string MPN;
            public string color;
            public string yieldTxt;
            public int    yieldCnt;
            public List<int> compat;

            public toner()
            {
                compat = new List<int>();
            }
        }

        private class printer {
            [JsonIgnore]
            public bool remove;
            [JsonIgnore]
            public string link;
            [JsonIgnore]
            public bool noSearch;

            public int      index;
            public string   modelname;
            public string   vendor;
            public bool     isColor;
            public List<toner> toners;

            public printer()
            {
                noSearch = false;
                remove = false;
                toners = new List<toner>();
            }
        }

        static private List<printer> printers = new List<printer>();
        static private Dictionary<string, List<int>> tnLookup = new Dictionary<string, List<int>>();


        static int index = 1;

        static string root = "http://www.inkgrabber.com/{0}";
        static string fileRoot = @"c:\tmp\Printers_InkGrabber\";
        //static string root = "http://abc-ink.stores.yahoo.net/{0}";

        
        [STAThread]
        static void Main()
        {
            string filePath, link;
            List<printer> tmp = new List<printer>();


            filePath = Path.Combine(fileRoot, "Brother");
            link = "BROTHER_menu.html";
            tmp = DoWeb(link, filePath, Path.Combine(filePath, "Brother.json"), "Brother");
            foreach (var x in tmp) printers.Add(x);

            filePath = Path.Combine(fileRoot, "Canon");
            link = "CANON_menu.html";
            tmp = DoWeb(link, filePath, null, "Canon");
            foreach (var x in tmp) printers.Add(x);

            filePath = Path.Combine(fileRoot, "Dell");
            link = "DELL_menu.html";
            tmp = DoWeb(link, filePath, null, "Dell");
            foreach (var x in tmp) printers.Add(x);

            filePath = Path.Combine(fileRoot, "Epson");
            link = "EPSON_menu.html";
            tmp = DoWeb(link, filePath, null, "Epson");
            foreach (var x in tmp) printers.Add(x);

            filePath = Path.Combine(fileRoot, "HP");
            link = "HP_menu.html";
            tmp = DoWeb(link, filePath, null, "HP");
            foreach (var x in tmp) printers.Add(x);

            filePath = Path.Combine(fileRoot, "Kodak");
            link = "KODAK_menu.html";
            tmp = DoWeb(link, filePath, null, "Kodak");
            foreach (var x in tmp) printers.Add(x);

            filePath = Path.Combine(fileRoot, "Lexmark");
            link = "LEXMARK_menu.html";
            tmp = DoWeb(link, filePath, null, "Lexmark");
            foreach (var x in tmp) printers.Add(x);

            filePath = Path.Combine(fileRoot, "KM");
            link = "MINOLTA_menu.html";
            tmp = DoWeb(link, filePath, null, "Konica Minolta");
            foreach (var x in tmp) printers.Add(x);

            filePath = Path.Combine(fileRoot, "NEC");
            link = "NEC_menu.html";
            tmp = DoWeb(link, filePath, null, "NEC");
            foreach (var x in tmp) printers.Add(x);

            filePath = Path.Combine(fileRoot, "Oki");
            link = "OKIDATA_menu.html";
            tmp = DoWeb(link, filePath, null, "Oki");
            foreach (var x in tmp) printers.Add(x);

            filePath = Path.Combine(fileRoot, "Panasonic");
            link = "PANASONIC_menu.html";
            tmp = DoWeb(link, filePath, null, "Panasocic");
            foreach (var x in tmp) printers.Add(x);

            filePath = Path.Combine(fileRoot, "Ricoh");
            link = "Ricoh_menu.html";
            tmp = DoWeb(link, filePath, null, "Ricoh");
            foreach (var x in tmp) printers.Add(x);

            filePath = Path.Combine(fileRoot, "Samsung");
            link = "SAMSUNG_menu.html";
            tmp = DoWeb(link, filePath, null, "Samsung");
            foreach (var x in tmp) printers.Add(x);

            filePath = Path.Combine(fileRoot, "Sharp");
            link = "SHARP_menu.html";
            tmp = DoWeb(link, filePath, null, "Sharp");
            foreach (var x in tmp) printers.Add(x);

            filePath = Path.Combine(fileRoot, "Toshiba");
            link = "TOSHIBA_menu.html";
            tmp = DoWeb(link, filePath, null, "Toshiba");
            foreach (var x in tmp) printers.Add(x);

            filePath = Path.Combine(fileRoot, "Xerox");
            link = "XEROX_menu.html";
            tmp = DoWeb(link, filePath, null, "Xerox");
            foreach (var x in tmp) printers.Add(x);

            foreach (var prt in printers)
            {
                foreach (var tn in prt.toners)
                {
                    if (!tnLookup.ContainsKey(tn.descr))
                    {
                        List<int> tnlist = new List<int>();
                        tnLookup.Add(tn.descr, tnlist);
                    }
                    tnLookup[tn.descr].Add(prt.index);
                }
            }
            foreach (var prt in printers)
            {
                foreach (var tn in prt.toners)
                {
                    tn.compat = tnLookup[tn.descr];
                    tn.compat = new List<int>(tnLookup[tn.descr]);
                    tn.compat.Remove(prt.index);
                }
            }

            string config = "All.json";
            filePath = fileRoot;
            using (FileStream fs = File.Open(Path.Combine(filePath, config), FileMode.Create, FileAccess.Write))
            {
                byte[] buffer = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(printers));
                fs.Write(buffer, 0, buffer.Length);
            }
        }

        static HtmlDocument getAndStore(string link, string filePath)
        {
            HtmlDocument doc = new HtmlDocument();
            if (link.Contains('/'))     // referral link
                return null;
            string file = Path.Combine(filePath, link);
            bool diskFile = true;
            if (File.Exists(file))
                doc.Load(Path.Combine(filePath, link));
            else
            {
                doc = new HtmlWeb().Load(String.Format(root, link));
                diskFile = false;
            }
            if (!diskFile && doc != null)
            {
                string html = doc.DocumentNode.OuterHtml;
                if (!Directory.Exists(filePath))
                    Directory.CreateDirectory(filePath);

                using (FileStream fs = File.Open(file, FileMode.Create, FileAccess.Write))
                {
                    byte[] buffer = Encoding.UTF8.GetBytes(html);
                    fs.Write(buffer, 0, buffer.Length);
                }
            }
            return doc;
        }

        private static List<printer> DoWeb(string link, string filePath, string config, string vendor)
        {

            HtmlDocument doc;
            List<printer> printers = new List<printer>();

            if (link != null)
            {
                doc = getAndStore(link, filePath);

                HtmlNodeCollection data = doc.DocumentNode.SelectNodes("//td");
//                HtmlNodeCollection data = doc.DocumentNode.SelectNodes("//div[@class='greybar']");

                foreach (HtmlNode node in data)
                {
                    HtmlNodeCollection hrefs = node.SelectNodes("a/@href");
                    if (hrefs != null)
                    {
                        HtmlNode href = hrefs.First();
                        if (href.InnerText.Contains("Fax"))
                            continue;
                        printer prt = new printer();
                        prt.link = href.Attributes["href"].Value;
                        if (prt.link.Contains("Fax"))
                            continue;
                        prt.modelname = vendor + " " + href.InnerText;
                        prt.vendor = vendor.ToUpper();
                        prt.index = index;
                        prt.isColor = false;
                        printers.Add(prt);
                        index++;
                        Console.WriteLine("Found: {0}", prt.modelname);
                    }
                }

                foreach (var prt in printers)
                {
                    doc = getAndStore(prt.link, filePath);
                    if (doc == null)
                        continue;
                    data = doc.DocumentNode.SelectNodes("//input[@type='hidden' and @name='Item_Name']");
                    if (data == null)
                        continue;
                    foreach (HtmlNode node in data)
                    {
                        string x= null;
                        int index1 = 0, index2 = 0;
                        if (node.Attributes.Count == 3)
                           x = node.Attributes[2].Value;
                        if (x != null)
                        {
                            if (x.Contains("ribbon") || x.Contains("Ribbon"))
                            {
                                prt.remove = true;
                                break;
                            }
                            if (x.Contains("Genuine") && !x.Contains("Replacement"))
                            {
                                toner tn = new toner();
                                x = x.Replace("up to ", "");
                                if (x.EndsWith("pages"))
                                    x = x + ")";

                                string x1 = String.Copy(x);
                                tn.descr = x1;
                                x = x.ToLower();
                                if (x.Contains("color")) 
                                    tn.color = "color";
                                if (x.Contains("cyan")) 
                                    tn.color = "cyan";
                                if (x.Contains("magenta")) 
                                    tn.color = "magenta";
                                if (x.Contains("yellow")) 
                                    tn.color = "yellow";
                                if (!String.IsNullOrEmpty(tn.color))
                                    prt.isColor = true;
                                if (x.Contains("black"))
                                    tn.color = "black";

                                if (x.Count(ch => ch == '(') == 1)
                                {
                                    if ((index1 = x.IndexOf("(", 0)) > 0)
                                    {
                                        if ((index2 = x.IndexOf(")", index1)) > 0)
                                        {
                                            string y = x1.Substring(index1 + 1, index2 - index1 - 1);
                                            if (y.ToLower().Contains("pages") || y.ToLower().Contains("yield"))
                                            {
                                                tn.MPN = String.Empty;
                                                tn.yieldTxt = y;
                                                string number = string.Join("", y.ToCharArray().Where(Char.IsDigit));
                                                int val = 0;
                                                Int32.TryParse(number, out val);
                                                tn.yieldCnt = val;
                                            }
                                            else
                                            {
                                                tn.yieldTxt = String.Empty;
                                                tn.yieldCnt = 0;
                                                tn.MPN = y;
                                            }
                                        }
                                    }
                                }
                                else
                                if ((index1 = x.IndexOf("(", 0)) > 0)
                                {
                                    if ((index2 = x.IndexOf(")", index1)) > 0)
                                    {
                                        tn.MPN = x1.Substring(index1 + 1, index2 - index1 - 1);
                                        tn.MPN = tn.MPN.Replace("OEM", "");
                                    }
                                    if ((index1 = x.IndexOf("(", index1 + 1)) > 0)
                                    {
                                        if ((index2 = x.IndexOf(")", index1)) > 0)
                                        {
                                            string y = x1.Substring(index1 + 1, index2 - index1 - 1);
                                            tn.yieldTxt = y;
                                            if (y.ToLower().Contains("pages") || y.ToLower().Contains("yield"))
                                            {
                                                string number = string.Join("", y.ToCharArray().Where(Char.IsDigit));
                                                int val = 0;
                                                Int32.TryParse(number, out val);
                                                tn.yieldCnt = val;
                                            }
                                        }
                                    }
                                }
                                prt.toners.Add(tn);
                                Console.WriteLine("Found Toner: {0}", tn.descr);
                            }
                        }
                    }
                }

                printers = printers.Where(p => p.remove == false).ToList();
            }
            if (config != null)
            {
                using (FileStream fs = File.Open(Path.Combine(filePath, config), FileMode.Create, FileAccess.Write))
                {
                    byte[] buffer = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(printers));
                    fs.Write(buffer, 0, buffer.Length);
                }
            }
            return printers;
        }

    }
}